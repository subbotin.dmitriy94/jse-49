package com.tsconsulting.dsubbotin.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IReceiverService {

    @SneakyThrows
    void receive(@NotNull MessageListener listener);

}
