package com.tsconsulting.dsubbotin.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum EntityOperationType {

    POST_LOAD("Post load"),
    POST_PERSIST("Post persist"),
    POST_REMOVE("Post remove"),
    POST_UPDATE("Post update"),
    PRE_PERSIST("Pre persist"),
    PRE_REMOVE("Pre remove"),
    PRE_UPDATE("Pre update");

    private final String name;

}
