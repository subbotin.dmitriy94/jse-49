package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.entity.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.entity.Session;
import com.tsconsulting.dsubbotin.tm.entity.User;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.repository.entity.SessionRepository;
import com.tsconsulting.dsubbotin.tm.repository.entity.UserRepository;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.util.Date;

public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Session session;

    @NotNull
    private final String sessionId;

    @NotNull
    private final EntityManager entityManager;

    public SessionRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
        sessionRepository = new SessionRepository(entityManager);
        userRepository = new UserRepository(entityManager);
        @NotNull final User user = new User();
        user.setLogin("guest");
        @NotNull final String password = "guest";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(iteration, secret, password));
        entityManager.getTransaction().begin();
        userRepository.create(user);
        entityManager.getTransaction().commit();
        session = new Session();
        sessionId = session.getId();
        session.setUser(user);
        session.setDate(new Date());
    }

    @Test
    public void openClose() throws AbstractException {
        entityManager.getTransaction().begin();
        sessionRepository.open(session);
        entityManager.getTransaction().commit();
        @NotNull final Session foundSession = sessionRepository.findById(sessionId);
        Assert.assertEquals(session.getId(), foundSession.getId());
        Assert.assertEquals(session.getUser().getId(), foundSession.getUser().getId());
        Assert.assertEquals(session.getSignature(), foundSession.getSignature());
        entityManager.getTransaction().begin();
        sessionRepository.close(session);
        entityManager.getTransaction().commit();
        Assert.assertFalse(sessionRepository.existByUserId(session.getUser().getId()));
    }

    @Test
    public void contains() {
        entityManager.getTransaction().begin();
        sessionRepository.open(session);
        entityManager.getTransaction().commit();
        Assert.assertTrue(sessionRepository.contains(session));
    }

    @After
    public void finalizeTest() {
        entityManager.getTransaction().begin();
        sessionRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        userRepository.remove(session.getUser());
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
