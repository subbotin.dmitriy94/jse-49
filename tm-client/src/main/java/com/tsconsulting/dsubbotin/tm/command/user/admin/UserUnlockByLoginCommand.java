package com.tsconsulting.dsubbotin.tm.command.user.admin;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserUnlockByLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    @NotNull
    public String description() {
        return "User unlock by login.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        endpointLocator
                .getAdminUserEndpoint()
                .unlockByLoginUser(session, login);
        TerminalUtil.printMessage(String.format("%s user unlocked.", login));
    }

}
