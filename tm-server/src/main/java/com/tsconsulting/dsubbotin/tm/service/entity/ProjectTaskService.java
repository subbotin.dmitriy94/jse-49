package com.tsconsulting.dsubbotin.tm.service.entity;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.entity.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.entity.IProjectTaskService;
import com.tsconsulting.dsubbotin.tm.entity.Project;
import com.tsconsulting.dsubbotin.tm.entity.Task;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.repository.entity.ProjectRepository;
import com.tsconsulting.dsubbotin.tm.repository.entity.TaskRepository;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

@RequiredArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final ILogService logService;

    @Override
    public void bindTaskToProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            isEmpty(projectRepository, taskRepository, userId, projectId, taskId);
            @NotNull final Task task = taskRepository.findById(userId, taskId);
            @NotNull final Project project = projectRepository.findById(userId, projectId);
            task.setProject(project);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            isEmpty(projectRepository, taskRepository, userId, projectId, taskId);
            @NotNull final Task task = taskRepository.findById(userId, taskId);
            task.setProject(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<Task> findAllTasksByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAllByProjectId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, id);
            entityManager.getTransaction().begin();
            for (Task task : tasks) task.setProject(null);
            @NotNull final Project project = projectRepository.findById(userId, id);
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        final int realIndex = index - 1;
        if (realIndex < 0) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = projectRepository.findByIndex(userId, realIndex);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, project.getId());
            entityManager.getTransaction().begin();
            for (Task task : tasks) task.setProject(null);
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = projectRepository.findByName(userId, name);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, project.getId());
            entityManager.getTransaction().begin();
            for (Task task : tasks) task.setProject(null);
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllProject(@NotNull String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull List<Project> projects = projectRepository.findAll(userId);
            entityManager.getTransaction().begin();
            for (Project project : projects) taskRepository.removeAllTaskByProjectId(userId, project.getId());
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    private void isEmpty(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(userId)) throw new EmptyIdException();
        if (EmptyUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (EmptyUtil.isEmpty(taskId)) throw new EmptyIdException();
        projectRepository.findById(userId, projectId);
        taskRepository.findById(userId, taskId);
    }

}
