package com.tsconsulting.dsubbotin.tm.service.dto;

import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.dto.IOwnerDtoService;
import com.tsconsulting.dsubbotin.tm.dto.AbstractOwnerEntityDTO;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractOwnerDtoService<E extends AbstractOwnerEntityDTO> extends AbstractDtoService<E> implements IOwnerDtoService<E> {

    public AbstractOwnerDtoService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

}
