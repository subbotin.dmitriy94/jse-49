package com.tsconsulting.dsubbotin.tm;

import com.tsconsulting.dsubbotin.tm.endpoint.*;
import com.tsconsulting.dsubbotin.tm.marker.SoapCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint;

    public SessionEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
        adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
    }

    @Test
    @Category(SoapCategory.class)
    public void openCloseSession() throws AbstractException_Exception {
        @NotNull final SessionDTO sessionTemp = sessionEndpoint.openSession("user", "user");
        Assert.assertNotNull(sessionTemp);
        Assert.assertTrue(sessionEndpoint.closeSession(sessionTemp));
    }

    @Test
    @Category(SoapCategory.class)
    public void register() throws AbstractException_Exception {
        @NotNull final SessionDTO sessionTemp = sessionEndpoint.register("test", "test", "aa@a.com");
        Assert.assertNotNull(sessionTemp);
        Assert.assertTrue(sessionEndpoint.closeSession(sessionTemp));
        @NotNull final SessionDTO sessionAdmin = sessionEndpoint.openSession("admin", "admin");
        adminUserEndpoint.removeByLoginUser(sessionAdmin, "test");
        sessionEndpoint.closeSession(sessionAdmin);
    }

}
