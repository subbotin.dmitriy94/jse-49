package com.tsconsulting.dsubbotin.tm.component;

import com.tsconsulting.dsubbotin.tm.dto.EntityLogDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.EntityOperationType;
import com.tsconsulting.dsubbotin.tm.service.ActiveMqService;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageExecutor {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final ActiveMqService service = new ActiveMqService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final EntityOperationType type) {
        es.submit(() -> {
            @NotNull final EntityLogDTO entity;
            entity = service.createMessage(object, type);
            service.send(entity);
        });
    }

    public void stop() {
        es.shutdown();
    }

}
