package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-clear";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        endpointLocator.getTaskEndpoint().removeAllTask(session);
        TerminalUtil.printMessage("[Clear]");
    }

}
