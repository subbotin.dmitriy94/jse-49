package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.ILoggerService;
import com.tsconsulting.dsubbotin.tm.dto.EntityLogDTO;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class LoggerService implements ILoggerService {

    @Override
    public void writeLog(@NotNull final EntityLogDTO message) throws IOException {
        @NotNull final File file = getFile(message.getClassName());
        createPath(file.getParentFile());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        @NotNull final String header = getHeader(message);
        fileOutputStream.write(header.getBytes());
        fileOutputStream.write(message.getEntity().getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @NotNull
    private String getHeader(@NotNull final EntityLogDTO message) {
        return String.format(
                "Class: %s; Date: %s; Operation type: %s\n",
                message.getClassName(),
                message.getDate(),
                message.getOperationType()
        );
    }

    @NotNull
    private File getFile(@NotNull final String className) {
        @NotNull final String fileName = "./logger/" + className.replace("DTO", "") + ".log";
        return new File(fileName);
    }

    private void createPath(@NotNull final File file) {
        file.mkdirs();
    }

}
