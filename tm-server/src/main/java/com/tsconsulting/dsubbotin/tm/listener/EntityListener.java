package com.tsconsulting.dsubbotin.tm.listener;

import com.tsconsulting.dsubbotin.tm.component.Bootstrap;
import com.tsconsulting.dsubbotin.tm.enumerated.EntityOperationType;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

import static com.tsconsulting.dsubbotin.tm.enumerated.EntityOperationType.*;

public class EntityListener {

    @PostLoad
    public void postLoad(@NotNull final Object entity) {
        sendMessage(entity, POST_LOAD);
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity) {
        sendMessage(entity, POST_PERSIST);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity) {
        sendMessage(entity, POST_REMOVE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, POST_UPDATE);
    }

    @PrePersist
    public void prePersist(@NotNull final Object entity) {
        sendMessage(entity, PRE_PERSIST);
    }

    @PreRemove
    public void preRemove(@NotNull final Object entity) {
        sendMessage(entity, PRE_REMOVE);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object entity) {
        sendMessage(entity, PRE_UPDATE);
    }

    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operation) {
        Bootstrap.sendMessage(entity, operation);
    }

}
