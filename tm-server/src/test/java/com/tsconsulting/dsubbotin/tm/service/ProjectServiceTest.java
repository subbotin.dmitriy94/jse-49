package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.entity.IProjectService;
import com.tsconsulting.dsubbotin.tm.api.service.entity.IUserService;
import com.tsconsulting.dsubbotin.tm.entity.Project;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.service.entity.ProjectService;
import com.tsconsulting.dsubbotin.tm.service.entity.UserService;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public final class ProjectServiceTest {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "projectName";

    @NotNull
    private final String projectDescription = "projectDescription";

    @NotNull
    private final String userId;

    public ProjectServiceTest() throws AbstractException {
        @NotNull final PropertyService propertyService = new PropertyService();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        @NotNull final LogService logService = new LogService();
        projectService = new ProjectService(connectionService, logService);
        userService = new UserService(connectionService, logService);
        userId = userService.create("test", "test").getId();
        projectId = projectService.create(userId, projectName, projectDescription).getId();
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        @NotNull final Project newProject = projectService.create(userId, newProjectName, newProjectDescription);
        Assert.assertNotNull(projectService.findByName(userId, projectName));
        Assert.assertEquals(newProject.getName(), newProjectName);
        Assert.assertEquals(newProject.getDescription(), newProjectDescription);
        projectService.clear(userId);
    }

    @Test
    public void findByName() throws AbstractException {
        @NotNull final Project project = projectService.findByName(userId, projectName);
        Assert.assertEquals(project.getName(), projectName);
        Assert.assertEquals(project.getDescription(), projectDescription);
        Assert.assertEquals(project.getUser().getId(), userId);
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectService.updateById(userId, projectId, newProjectName, newProjectDescription);
        @NotNull final Project tempProject = projectService.findByName(userId, newProjectName);
        Assert.assertNotEquals(tempProject.getName(), projectName);
        Assert.assertNotEquals(tempProject.getDescription(), projectDescription);
        Assert.assertEquals(tempProject.getName(), newProjectName);
        Assert.assertEquals(tempProject.getDescription(), newProjectDescription);
        Assert.assertEquals(tempProject.getId(), projectId);
        Assert.assertEquals(tempProject.getUser().getId(), userId);
    }

    @Test
    public void updateByIndex() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectService.updateByIndex(userId, 1, newProjectName, newProjectDescription);
        @NotNull final Project tempProject = projectService.findByName(userId, newProjectName);
        Assert.assertNotEquals(tempProject.getName(), projectName);
        Assert.assertNotEquals(tempProject.getDescription(), projectDescription);
        Assert.assertEquals(tempProject.getName(), newProjectName);
        Assert.assertEquals(tempProject.getDescription(), newProjectDescription);
        Assert.assertEquals(tempProject.getId(), projectId);
        Assert.assertEquals(tempProject.getUser().getId(), userId);
    }

    @Test
    public void startById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findById(userId, projectId);
        Assert.assertNull(tempProject.getStartDate());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startById(userId, projectId);
        @NotNull final Project updProject = projectService.findById(userId, projectId);
        Assert.assertNotNull(updProject.getStartDate());
        Assert.assertEquals(updProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        Assert.assertNull(projectService.findByIndex(userId, 1).getStartDate());
        Assert.assertEquals(projectService.findByIndex(userId, 1).getStatus(), Status.NOT_STARTED);
        projectService.startByIndex(userId, 1);
        Assert.assertNotNull(projectService.findByIndex(userId, 1).getStartDate());
        Assert.assertEquals(projectService.findByIndex(userId, 1).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByName(userId, projectName);
        Assert.assertNull(tempProject.getStartDate());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startByName(userId, projectName);
        @NotNull final Project updTempProject = projectService.findByName(userId, projectName);
        Assert.assertNotNull(updTempProject.getStartDate());
        Assert.assertEquals(updTempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException {
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.NOT_STARTED);
        projectService.finishById(userId, projectId);
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        Assert.assertEquals(projectService.findByIndex(userId, 1).getStatus(), Status.NOT_STARTED);
        projectService.finishByIndex(userId, 1);
        Assert.assertEquals(projectService.findByIndex(userId, 1).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException {
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.NOT_STARTED);
        projectService.finishByName(userId, projectName);
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.NOT_STARTED);
        projectService.updateStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.IN_PROGRESS);
        projectService.updateStatusById(userId, projectId, Status.COMPLETED);
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.COMPLETED);
        projectService.updateStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        Assert.assertEquals(projectService.findByIndex(userId, 1).getStatus(), Status.NOT_STARTED);
        projectService.updateStatusByIndex(userId, 1, Status.IN_PROGRESS);
        Assert.assertEquals(projectService.findByIndex(userId, 1).getStatus(), Status.IN_PROGRESS);
        projectService.updateStatusByIndex(userId, 1, Status.COMPLETED);
        Assert.assertEquals(projectService.findByIndex(userId, 1).getStatus(), Status.COMPLETED);
        projectService.updateStatusByIndex(userId, 1, Status.NOT_STARTED);
        Assert.assertEquals(projectService.findByIndex(userId, 1).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.NOT_STARTED);
        projectService.updateStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.IN_PROGRESS);
        projectService.updateStatusByName(userId, projectName, Status.COMPLETED);
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.COMPLETED);
        projectService.updateStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.NOT_STARTED);
    }

    @After
    public void finalizeTest() throws AbstractException {
        projectService.clear(userId);
        userService.removeById(userId);
    }

}
