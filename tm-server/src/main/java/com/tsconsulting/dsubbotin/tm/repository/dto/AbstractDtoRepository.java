package com.tsconsulting.dsubbotin.tm.repository.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.IDtoRepository;
import com.tsconsulting.dsubbotin.tm.dto.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public abstract class AbstractDtoRepository<E extends AbstractEntityDTO> implements IDtoRepository<E> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractDtoRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void create(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        entityManager.remove(entity);
    }

    @Override
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

    @Override
    public void clear() {
        entityManager.clear();
    }

}
