package com.tsconsulting.dsubbotin.tm.entity;

import com.tsconsulting.dsubbotin.tm.listener.EntityListener;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sessions")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractEntity implements Cloneable {

    @Column
    @NotNull
    private Date date;

    @Nullable
    @Column
    private String signature;

    @NotNull
    @ManyToOne
    private User user;

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }


}
